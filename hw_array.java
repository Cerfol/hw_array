package hw_array;
public class hw_array {
	
	public static void main(String[] args) {

	    int[] c = {1500, 50, 500, 700, 450, 30, 2000, 300};
	    
	    for (int i = 0; i < c.length; i++)
	        for (int s = c.length-1; s > i; s--)
	         
	            if (c[s] > c[s - 1])
	                swap(c, s, s - 1);
	            
	    {
	    
	         for (float a : c) {
	        	 
				a=(float)(a*1.1);
				
	        System.out.print(Math.round(a) + "  ");
	    
	        }
	    }
	}

	static void swap(int[] p, int left, int right) {
	    if (left != right) {
	        int temp = p[left];
	        p[left] = p[right];
	        p[right] = temp;
	    }
	}
}